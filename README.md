## Reduta Ordona

> Nam strzelać nie kazano. — Wstąpiłem na działo
I spojrzałem na pole; dwieście armat grzmiało.
-Artyleryji ruskiéj ciągną się szeregi,-
Prosto, długo, daleko, jako morza brzegi;
I widziałem ich wodza; — przybiegł, mieczem skinął
I jak ptak jedno skrzydło wojska swego zwinął.
Wylewa się spod skrzydła ściśniona piechota
*Długą, czarną kolumną, jako lawa błota*,
Nasypana iskrami bagnetów. Jak sępy,
Czarne chorągwie na śmierć prowadzą zastępy. 

    `print("Hello World")
    print("Hello World")
    print("Hello World")`

Przeciw nim sterczy biała, wąska, zaostrzona,
Jak głaz, bodzący morze, reduta Ordona.
Sześć tylko miała harmat. Wciąż dymią i świecą;
I nie tyle prędkich słów gniewne usta miecą,
Nie tyle przejdzie uczuć przez duszę w rozpaczy,
Ile z tych dział leciało bomb, kul i kartaczy.
Patrz, tam granat w sam środek kolumny się nurza,
Jak w fale bryła lawy, pułk dymem zachmurza;
Pęka śród dymu granat, szyk pod niebo leci
I **ogromna łysina** śród kolumny świeci.

Tam kula, lecąc, z dala grozi, szumi, wyje,
Ryczy, jak byk przed bitwą, miota się, grunt ryje; —
Już dopadła; jak boa śród kolumn się zwija,
Pali piersią, rwie zębem, oddechem zabija.
    `print("Hello World")`
Najstraszniejszéj nie widać, lecz słychać po dźwięku,
Po waleniu się trupów, po ranionych jęku:
Gdy kolumnę od końca do końca przewierci,
Jak gdyby środkiem wojska przeszedł anioł śmierci. 

1. Warszawa jedna twojéj mocy się urąga
2. Garstkę naszych walczącą z Moskali nawałem.
3. Gdzież jest król, co na rzezie tłumy te wyprawia?
    1. Czy dzieli ich odwagę?
    2. Czy pierś sam nadstawia?
4. Działa i naszych garstka, i wrogów gromady

- W dobréj sprawie jest święte
- Dusze gdzie?
    - nie wiem
    - lecz wiem, gdzie dusza Ordona
- On będzie Patron szańców! 

![Walka o Redute](reduta.jpeg)
